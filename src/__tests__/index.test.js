const { sum, main } = require("../index");

test("adds 1 + 2 to equal 3", () => {
  expect(sum(1, 2)).toBe(3);
});

test("Test main function success", () => {
  expect(main(4)).toBe("Application working fine! 🎉");
});

test("Test main function crashed", () => {
  expect(main(2)).toBe("Application crashed 🤯");
});
