// @flow

export function main(check: number): string {
  if (check === 4) {
    return "Application working fine! 🎉";
  } else {
    return "Application crashed 🤯";
  }
}

export function sum(a: number, b: number): number {
  return a + b;
}
